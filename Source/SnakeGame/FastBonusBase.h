// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "FastBonusBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AFastBonusBase : public ABonusBase
{
	GENERATED_BODY()

public:

	AFastBonusBase();
	
};

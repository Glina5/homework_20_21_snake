// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	MeshWallComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshWallComponent"));
	MeshWallComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	MeshWallComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshWallComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	WallGame = true;
}

void AWallBase::WallGameAct_Implementation()
{
	MeshWallComponent->SetVisibility(false,true);///////////// �����������!!!!!!!!!!!!!!!!
}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		auto Wall = this;
		if (IsValid(Snake))
		{
			UE_LOG(LogTemp, Warning, TEXT("WallGame, %d"), static_cast<int>(WallGame));
			if (WallGame)
			{
			Snake->SnakeElements[0]->SetActorLocation(Snake->SnakeElements[1]->GetActorLocation());
			Snake->SnakeElements[1]->MeshComponent->SetVisibility(false,false);
			Snake->SnakeElements[0]->SnakeDefeat(); // ������ ����� ����������
			Snake->SnakeDead = true; // ������ ���������� ���������, � ����� ������� ��������
			}
			else // ��������� ������������ � ��������������� ����� �����
			{
				float X_ = this->GetActorLocation().X;
				float Y_ = this->GetActorLocation().Y;
				float Z_ = this->GetActorLocation().Z;
				if (Snake->LastMoveDirection == EMovementDirection::UP)
				{
					Snake->SnakeElements[0]->SetActorLocation(FVector(X_-Snake->MapTeleport_Y,Y_,Z_));
				}
				else if(Snake->LastMoveDirection == EMovementDirection::DOWN)
				{
					Snake->SnakeElements[0]->SetActorLocation(FVector(X_ + Snake->MapTeleport_Y, Y_, Z_));
				}
				else if (Snake->LastMoveDirection == EMovementDirection::LEFT)
				{
					Snake->SnakeElements[0]->SetActorLocation(FVector(X_, Y_ - Snake->MapTeleport_X, Z_));
				}
				else if (Snake->LastMoveDirection == EMovementDirection::RIGHT)
				{
					Snake->SnakeElements[0]->SetActorLocation(FVector(X_ , Y_ + Snake->MapTeleport_X, Z_));
				}
			}
		}
	}
}


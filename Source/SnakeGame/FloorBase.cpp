// Fill out your copyright notice in the Description page of Project Settings.


#include "FloorBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFloorBase::AFloorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	MeshFloorComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshFloorComponent"));
	MeshFloorComponent->AttachTo(Root);
	MeshFloorComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

// Called when the game starts or when spawned
void AFloorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFloorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFloorBase::SelectMaterial_Implementation()
{

}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
class ABonusBase;
class AFastBonusBase;
class ASlowBonusBase;
class APlayerPawnBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

	bool SpawnSnake; // True ������ ��� ������� ������ SnakeElements
	int32 Progress;	// ���������� �����, ������� ����� ������ ������ ������
	float SpeedBonus;	// ����������� (���������) ��������� �������� ������ �� ������
	template <class T>
	void ActorCheck(T& CheckActor);	// ������, ��� �������� �� ������� ��������� ����, 
									//��������� ��� ��������� ������, ��� � �����.
	template <class T>
	void ActorKill(T& KilledActor); // ������, ��� Destroy ������� �� �������

public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	TArray<ABonusBase*> BonusElements;

	UPROPERTY()
	TArray<FVector> FloorLocation;	// ���������� ������ ����

	UPROPERTY()
	TArray<FVector> EmptyLocation;	// ���������� "������" ������ ���� 
									// ������ - ������ ��� �������� ���: ������, ���, ������.

	UPROPERTY()
	TArray<AFood*> FoodElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY()
	EMovementDirection CheckMoveDirection; // ������ LastMoveDirection, ��� ����������� ��������

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFastBonusBase> FastBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASlowBonusBase> SlowBonusClass;

	UPROPERTY()
	APlayerPawnBase* PlayerPawn;

	UPROPERTY(BlueprintReadWrite)
	FString LevelName;	// ��� ������. ��� Blueprint

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;	// ������ �������� ������

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	float MapTeleport_X;	// ��������� �� ������� ����� ���������������� ������ �� �����. 
							//� ������ ������ �� ������� (WallGame = False)

	UPROPERTY()	
	float MapTeleport_Y;	// ��������� �� ������� ����� ���������������� ������ �� ����.
							//� ������ ������ �� ������� (WallGame = False)

	UPROPERTY(EditDefaultsOnly)
	int32 InitialSnakeSize;

	UPROPERTY()
	int32 MapSize;

	UPROPERTY()
	int32 FoodCount;	// ��� ������������ �� �����

	UPROPERTY()
	int32 FoodForWin;	// ������� ����� ������ ��� ������

	UPROPERTY(EditDefaultsOnly)
	int32 ProgressTime; // // ����� ����� ������� ����� ������ ����� + 5*(����� ������)
	
	UPROPERTY()
	bool ImmortalSnake;		// ���� true - �� ������ ������� ����� � ��������� ����
							// ���� false - �� ������ ����� ��� ����� � �����

	UPROPERTY()
	bool SnakeDead;			// ���� true - �� ������ ���������� ���������, � ����� ������� ��������

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move(); //void Move(float DeltaTime);
	UFUNCTION()
	void CreateFood();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void SnakeGetBonus(float SpeedCoef);	// ����� �� ��������
	UFUNCTION()
	void SpawnBonus();
	UFUNCTION(BlueprintNativeEvent)
	void SnakeWin();				// ���������� � Blueprint'� ������
	void SnakeWin_Implementation();
	UFUNCTION(BlueprintCallable)
	void NextLevel();
	UFUNCTION()
	void MaxFoodWin(bool& MaxFood);		// ��������� ������������� ��������� ��� ��� ������
};

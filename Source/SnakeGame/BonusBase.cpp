// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	MeshBonusComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshBonusComponent"));
	MeshBonusComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	MeshBonusComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshBonusComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshBonus_Component = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshBonus_Component"));
	MeshBonus_Component->AttachToComponent(MeshBonusComponent, FAttachmentTransformRules::KeepRelativeTransform);
	MeshBonus_Component->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshBonus_Component->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshLifeComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshLifeComponent")); // ���������� �������� ����� "�����" (����� �������������)
	MeshLifeComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	MeshLifeComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SpeedBonus = 1.f;
	SpeedGame = 0.5f;
	BaseTick = 0.f;
	LTime = 1;
	Scale_X = MeshLifeComponent->GetRelativeScale3D().X;
	Scale_Y = MeshLifeComponent->GetRelativeScale3D().Y;
	Scale_Z = MeshLifeComponent->GetRelativeScale3D().Z;
	ProgressTime = 10;
}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	//SetActorTickInterval(SpeedGame);
	Scale_X = MeshLifeComponent->GetRelativeScale3D().X;
	Scale_Y = MeshLifeComponent->GetRelativeScale3D().Y;
	Scale_Z = MeshLifeComponent->GetRelativeScale3D().Z;
}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("DeltaTime, %f"), DeltaTime);
	BaseTick += DeltaTime;
	if (BaseTick >= SpeedGame) // ��������� � ����������, ��� � "SnakeBase": (SetActorTickInterval(MovementSpeed))
	{
		BaseTick = 0.f;
		LifeTime(LTime);
		LTime++;
	}
	float Rotate_Pitch = MeshBonusComponent->GetRelativeRotation().Pitch;
	float Rotate_Roll = MeshBonusComponent->GetRelativeRotation().Roll;
	float Rotate_Yaw = MeshBonusComponent->GetRelativeRotation().Yaw;
	if (Rotate_Roll >= 360.0f)
	{
		Rotate_Roll = 0;
	}
	FRotator NewRotator(Rotate_Pitch, Rotate_Yaw, Rotate_Roll + DeltaTime*50);
	MeshBonusComponent->SetRelativeRotation(NewRotator);		// �������� ���� - ��� �������. � �� �������� ��� "5" ��� � ����))
}


void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		auto Bonus = this;
		if (IsValid(Snake))
		{
			Snake->SnakeGetBonus(SpeedBonus);
			int32 ElementIndex;
			Snake->BonusElements.Find(Bonus, ElementIndex);
			Snake->BonusElements.RemoveAtSwap(ElementIndex);
			this->Destroy();
		}
	}
}

void ABonusBase::LifeTime(int32 Progress)
{
	if (Progress > ProgressTime)
	{
		int32 ElementIndex;
		SnakeOwner->BonusElements.Find(this, ElementIndex);
		SnakeOwner->BonusElements.RemoveAtSwap(ElementIndex);
		this->Destroy();
	}
	FVector NewScale(Scale_X - ((Scale_X / ProgressTime) * Progress), Scale_Y, Scale_Z);
	MeshLifeComponent->SetRelativeScale3D(NewScale);		// ��������� �������� ����� "�����" (����� �������������) ������
}


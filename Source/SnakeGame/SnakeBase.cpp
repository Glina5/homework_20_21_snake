// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "BonusBase.h"
#include "FastBonusBase.h"
#include "SlowBonusBase.h"
#include "PlayerPawnBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	CheckMoveDirection = LastMoveDirection;
	InitialSnakeSize = 3;
	SpawnSnake = true;
	LevelName = TEXT("One");
	ImmortalSnake = false;
	SnakeDead = false;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(InitialSnakeSize);
	SpawnSnake = false;
	Progress = 0;
	ProgressTime = 10;
	SpeedBonus = 1.f;
	FoodForWin = 10;
//	UE_LOG(LogTemp, Warning, TEXT("FoodCount, %d"), FoodCount);
//	CreateFood();

}

void ASnakeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	for (int i = 0; i < BonusElements.Num(); i++)
	{
		if (IsValid(BonusElements[i]))
		BonusElements[i]->Destroy();
	}
	for (int i = 0; i < FoodElements.Num(); i++)
	{
		if (IsValid(FoodElements[i]))
		FoodElements[i]->Destroy();
	}
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		if (IsValid(SnakeElements[i]))
			SnakeElements[i]->Destroy();
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(!SnakeDead) // ��������� ���� ������ ����
	Move(); // Move(DeltaTime);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(GetActorLocation()+NewLocation);
		FRotator NewRotator;
		if (!SpawnSnake) // ���������� ��� �������� ���. ����� ������ ���� ����� ����������.
		{
			NewTransform.SetLocation(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation());
			NewRotator = SnakeElements[SnakeElements.Num() - 1]->MeshComponent->GetRelativeRotation(); // �������� ������� ������������ �������� ������
		}
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->MeshComponent->SetRelativeRotation(NewRotator); // �������� ������� ������������ �������� ������
//		if (SnakeElements.Num() >= InitialSnakeSize)
//		{
//		NewSnakeElement->SetActorHiddenInGame(true);
//		}
		//NewSnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move() //void ASnakeBase::Move(float DeltaTime)
{
	if (Progress > 0)
		Progress--;
	else if (Progress == 0)
	{
		SetActorTickInterval(MovementSpeed);
		Progress--;
	}

	FVector MovementVector(0,0,0);
	//float MovementSpeedDelta = MovementSpeed * DeltaTime;

	switch(LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize; //MovementVector.X += MovementSpeedDelta;
//		SnakeElements[0]->MeshConnectionComponent->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize; //MovementVector.X -= MovementSpeedDelta;
//		SnakeElements[0]->MeshConnectionComponent->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize; //MovementVector.Y += MovementSpeedDelta;
//		SnakeElements[0]->MeshConnectionComponent->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize; //MovementVector.Y -= MovementSpeedDelta;
//		SnakeElements[0]->MeshConnectionComponent->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
		break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
//		SnakeElements[i]->SetActorHiddenInGame(false);
//		SnakeElements[i]->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		FRotator PrevRotator = PrevElement->MeshComponent->GetRelativeRotation(); // ����������� �������, ����� ���������� ��������������
		CurrentElement->MeshComponent->SetRelativeRotation(PrevRotator);		  //������ ������. ��� ����� ������ ��� ����������� �������� ����������.
//		CurrentElement->MeshConnectionComponent->SetVisibility(true, false);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	CheckMoveDirection = LastMoveDirection;

}

void ASnakeBase::CreateFood()
{
	if (SnakeElements.Num() == InitialSnakeSize + FoodForWin)	// �������� �� ������ � ������!
	{
		if (PlayerPawn->ChoiceLevel == ESnakeLevel::FIVE)
		{
			PlayerPawn->SnakeAllWin();
			this->Destroy();
			return;
		}
		else
		{
			SnakeWin();
		}
	}
	int32 MapPoint; // ��������� ����� �� �����
//	bool SpawnFood;
	bool SpawnBonus = false; //���� true - �� ������ ���, ����� ��������� �����
	SpawnBonus = (FMath::RandRange(0,99) < 10);	// ����������� ������������ ������ 10%

//	UE_LOG(LogTemp, Warning, TEXT("FoodCount, %d"), FoodCount);
	if (SnakeElements.Num() <= (MapSize - FoodCount))
	{
		for (int32 i = 0; i < FloorLocation.Num(); i++)
		{
			EmptyLocation.Add(FloorLocation[i]);
		}
		ActorCheck(SnakeElements);
		ActorCheck(FoodElements);
		ActorCheck(BonusElements);

/*

		for (int32 i = 0; i < SnakeElements.Num(); i++)
		{
			int32 ElementIndex;
			if (EmptyLocation.Find(SnakeElements[i]->GetActorLocation(), ElementIndex))
			{
				EmptyLocation.RemoveAtSwap(ElementIndex);
			}
		}
		for (int32 i = 0; i < FoodElements.Num(); i++)
		{
			int32 ElementIndex;
			if (EmptyLocation.Find(FoodElements[i]->GetActorLocation(), ElementIndex))
			{
				EmptyLocation.RemoveAtSwap(ElementIndex);
			}
		}
		for (int32 i = 0; i < BonusElements.Num(); i++)
		{
			int32 ElementIndex;
			if (EmptyLocation.Find(BonusElements[i]->GetActorLocation(), ElementIndex))
			{
				EmptyLocation.RemoveAtSwap(ElementIndex);
			}
		}
*/

		MapPoint = FMath::RandRange(0, (EmptyLocation.Num()-1));// ������������� ��������� ����� �� �����
		FTransform FloorTransform(EmptyLocation[MapPoint]);
		AFood* NewFoodElemet = GetWorld()->SpawnActor<AFood>(FoodClass, FloorTransform);
		FoodElements.Add(NewFoodElemet);
		EmptyLocation.RemoveAtSwap(MapPoint);

		if (SpawnBonus)
		{
			MapPoint = FMath::RandRange(0, (EmptyLocation.Num() - 1));
			if (FMath::RandRange(0, 1))
			{
				FTransform FloorTransform_(EmptyLocation[MapPoint]);
				ABonusBase* NewBonusElement = GetWorld()->SpawnActor<AFastBonusBase>(FastBonusClass, FloorTransform_);
				BonusElements.Add(NewBonusElement);
				NewBonusElement->SnakeOwner = this;
				NewBonusElement->ProgressTime = ProgressTime + 5 * static_cast<int32>(PlayerPawn->ChoiceLevel);
				NewBonusElement->SpeedGame = MovementSpeed;
				SpawnBonus = false;
//				SpawnFood = false;
			}
			else
			{
				FTransform FloorTransform_(EmptyLocation[MapPoint]);
				ABonusBase* NewBonusElement = GetWorld()->SpawnActor<ASlowBonusBase>(SlowBonusClass, FloorTransform_);
				BonusElements.Add(NewBonusElement);
				NewBonusElement->SnakeOwner = this;
				NewBonusElement->ProgressTime = ProgressTime + 5 * static_cast<int32>(PlayerPawn->ChoiceLevel);
				NewBonusElement->SpeedGame = MovementSpeed;
				SpawnBonus = false;
//				SpawnFood = false;
			}
		}

		EmptyLocation.Empty();
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SnakeGetBonus(float SpeedCoef)
{
	SetActorTickInterval(MovementSpeed*SpeedCoef);
	Progress = ProgressTime + 5*static_cast<int32>(PlayerPawn->ChoiceLevel); // ����� ����� ������� ����� ������ ����� + 5*(����� ������)
}

void ASnakeBase::SpawnBonus()
{

}

void ASnakeBase::SnakeWin_Implementation()
{

}

void ASnakeBase::NextLevel()
{
	ActorKill(SnakeElements);
	ActorKill(FoodElements);
	ActorKill(BonusElements);
	PlayerPawn->NextLevel();
	Destroy();
}

void ASnakeBase::MaxFoodWin(bool& MaxFood)
{
	if (MaxFood)
	FoodForWin = (MapSize - InitialSnakeSize); // FoodForWin ������ ������ ����� ������ �����
}


template <class T>
void ASnakeBase::ActorCheck(T& CheckActor)
{
	for (int32 i = 0; i < CheckActor.Num(); i++)
	{
		int32 ElementIndex;
		if (EmptyLocation.Find(CheckActor[i]->GetActorLocation(), ElementIndex))
		{
			EmptyLocation.RemoveAtSwap(ElementIndex);
		}
	}
}

template <class T>
void ASnakeBase::ActorKill(T& KilledActor)
{
	for (int32 i = 0; i < KilledActor.Num(); i++)
	{
		KilledActor[i]->Destroy();
	}
}

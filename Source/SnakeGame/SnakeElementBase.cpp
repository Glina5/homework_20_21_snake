// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshConnectionComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshConnectionComponent"));	// ��� ���������� ������� ������ ��� ����� ������ ��� ����������� �������� ��� ����������.
	MeshConnectionComponent->AttachToComponent(MeshComponent, FAttachmentTransformRules::KeepRelativeTransform);
	MeshConnectionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
//	MeshConnectionComponent->SetVisibility(false, false);
}

void ASnakeElementBase::SnakeDefeat_Implementation()
{

}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (Snake->ImmortalSnake) // ������ ����� ���������� ����� � ���������� ����
		{
			int32 ElementIndex;
			Snake->SnakeElements.Find(this,ElementIndex);
			for (int32 i = Snake->SnakeElements.Num() - 1; i >= ElementIndex; i--)
			{
				Snake->SnakeElements[i]->Destroy();
				Snake->SnakeElements.RemoveAtSwap(i);
			}
		}
		else // ������ ����� ��� ����� � �����
		{
			Snake->SnakeElements[0]->SetActorLocation(Snake->SnakeElements[1]->GetActorLocation());
			Snake->SnakeElements[1]->MeshComponent->SetVisibility(false, false);
			Snake->SnakeElements[0]->SnakeDefeat();	// ������ ����� ����������
			Snake->SnakeDead = true;	// ������ ���������� ���������, � ����� ������� ��������
		}
	}

}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}


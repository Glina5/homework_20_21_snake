// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusBase.generated.h"

class ASnakeBase;

// ����������� ����� ������. ������� ��� FastBonuseBase � SlowBonusBase

UCLASS(Abstract)
class SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()

		int32 LTime;
public:	
	// Sets default values for this actor's properties
	ABonusBase();

	UPROPERTY()
		ASnakeBase* SnakeOwner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshBonusComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshBonus_Component;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshLifeComponent;
	UPROPERTY()
		float SpeedBonus;	// ����������� (���������) ��������� �������� ������ �� ������
	UPROPERTY()
		float Scale_X;
	UPROPERTY()
		float Scale_Y;
	UPROPERTY()
		float Scale_Z;
	UPROPERTY()
		float SpeedGame;	// ����� ����� ������ � "SnakeBase": (SetActorTickInterval(MovementSpeed))
	UPROPERTY()
		float BaseTick;		// += DeltaTime;
	UPROPERTY()
		int32 ProgressTime;		// ����� ����� ������� ����� ������ ����� + 5*(����� ������)

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	virtual void LifeTime(int32 Progress);	// ��������� ������ ������ ������ �� ��� ����������� (������� �� ProgressTime)

};

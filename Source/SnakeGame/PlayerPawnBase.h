// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFloorBase;
class AWallBase;

UENUM()
enum class ESnakeLevel // ������� ������ (�����)
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
};

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

	int32 X_Map;	//������ ����� �� �����������
	int32 Y_Map;	//������ ����� �� ���������


public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFloorBase> FloorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallBase> WallClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESnakeLevel ChoiceLevel; // ����� ������

	UPROPERTY()
	TArray<AFloorBase*> Floors;	

	UPROPERTY()
	TArray<AWallBase*> Walls;

	UPROPERTY()
	float MovementSpeed;

	UPROPERTY()
	float SnakeMovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	float FloorSize;	// ������ �������� ����

	UPROPERTY()
	int32 MapSize;	// X_Map * Y_Map

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 FoodCount;	// ��� ������������ �� �����

	UPROPERTY(BlueprintReadWrite)
	int32 FoodForWin;	// ������� ����� ������ ��� ������

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool WallGame;	// ���� true - �� ����� �����������
					// ���� false - �� ������ ���� ���������

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool MaxFood;	// ���� true - �� ��� ������ ����� ������ ������������ ���-�� ���
						// ���� false - �� ��� ������ ����� ������ FoodForWin ���-�� ���

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ImmortalSnake; // ���� true - �� ������ ������� ����� � ��������� ����
						// ���� false - �� ������ ����� ��� ����� � �����


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void DrowLevel();

	UFUNCTION(BlueprintCallable)
	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION(BlueprintCallable)
	void HandlePlayerExitInput();

	UFUNCTION()
	void NextLevel();

	UFUNCTION(BlueprintNativeEvent)
	void SnakeAllWin();					// ���������� � Blueprint'� ������
	void SnakeAllWin_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void StartSnake();					// ���������� � Blueprint'� ������
	void StartSnake_Implementation();

	UFUNCTION(BlueprintCallable)
	void ChooseLevel(UPARAM(ref) int32& level);	// ��� ���������� UI � Widget Blueprint

	UFUNCTION(BlueprintCallable)
	void ChangeSpeed(UPARAM(ref) float& value); // ��� ���������� UI � Widget Blueprint
};

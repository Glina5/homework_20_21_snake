// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "FloorBase.h"
#include "WallBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	FloorSize = 54.f;
	ChoiceLevel = ESnakeLevel::ONE;
	X_Map = 11;
	Y_Map = 11;
	FoodCount = 1;
	FoodForWin = 10;
	WallGame = true;
	MapSize = X_Map * Y_Map;
	MaxFood = false;
	MovementSpeed = 0.5f;
	SnakeMovementSpeed = MovementSpeed;
	ImmortalSnake = false;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	StartSnake();
//	DrowLevel();
//	CreateSnakeActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Exit", IE_Released, this, &APlayerPawnBase::HandlePlayerExitInput);
}

void APlayerPawnBase::DrowLevel()
{
	switch (ChoiceLevel)
	{
	case ESnakeLevel::ONE:
		X_Map = 11;
		Y_Map = 11;
		SetActorLocation(FVector(0.f, 0.f, 1232.f)); //��������� ��������� ������ ��� ������
		break;
	case ESnakeLevel::TWO:
		X_Map = 15;
		Y_Map = 15;
		SetActorLocation(FVector(0.f, 0.f, 1232.f));
		break;
	case ESnakeLevel::THREE:
		X_Map = 21;
		Y_Map = 21;
		SetActorLocation(FVector(0.f, 0.f, 1500.f));
		break;
	case ESnakeLevel::FOUR:
		X_Map = 31;
		Y_Map = 21;
		SetActorLocation(FVector(0.f, 0.f, 1500.f));
		break;
	case ESnakeLevel::FIVE:
		X_Map = 41;
		Y_Map = 21;
		SetActorLocation(FVector(0.f, 0.f, 1700.f));
		break;
	default:
		X_Map = 11;
		Y_Map = 11;
		break;
	}
	int32 Offset_X = (X_Map - 1) / 2;
	int32 Offset_Y = (Y_Map - 1) / 2;
	for (int32 i = 0; i < X_Map*Y_Map; i++) // �������� ����
	{
		FVector NewLocation(((i / X_Map) - Offset_Y) * FloorSize, ((i % X_Map) - Offset_X) * FloorSize, 0);
		FTransform NewTransform(NewLocation);
		AFloorBase* NewFloor = GetWorld()->SpawnActor<AFloorBase>(FloorClass, NewTransform);
//		NewFloor->SnakeOwner = this;
		int32 ElementIndex = Floors.Add(NewFloor);
		if (i % 2)
			NewFloor->SelectMaterial(); // ��� �������� ���������� �����
	}
	for (int32 i = 0; i < Y_Map+2; i++) // �������� ����
	{
		for (int32 j = 0; j < X_Map + 2; j++)
		{
			FVector NewLocation(0,0,0);
			if (i == 0 || i == (Y_Map + 1))
			{
				NewLocation.X = (i - Offset_Y - 1) * FloorSize;
				NewLocation.Y = (j - Offset_X - 1) * FloorSize;
			}
			else if (j == 0 || j == (X_Map + 1))
			{
				NewLocation.X = (i - Offset_Y - 1) * FloorSize;
				NewLocation.Y = (j - Offset_X - 1) * FloorSize;
			}
			else
			{
				continue;
			}
		FTransform NewTransform(NewLocation);
		AWallBase* NewWall = GetWorld()->SpawnActor<AWallBase>(WallClass, NewTransform);
		NewWall->WallGame = WallGame;
		if (!WallGame)
		{
			NewWall->WallGameAct(); // ��� �������� ���������� ������ ����
		}
		Walls.Add(NewWall);
		}
	}
}

void APlayerPawnBase::CreateSnakeActor()
{
	FTransform Elevation;
	Elevation.SetLocation(FVector(0, 0, 0));
	//Elevation.SetRotation(FQuat(FRotator(0, 0, 0)));
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, Elevation);
	for (int32 i = 0; i < X_Map * Y_Map; i++)
	{
		SnakeActor->FloorLocation.Add(Floors[i]->GetActorLocation()); // ������� ���������� ���� � SnakeActor
	}

	switch (ChoiceLevel)
	{
	case ESnakeLevel::ONE:
		SnakeActor->LevelName = TEXT("One");
		break;
	case ESnakeLevel::TWO:
		SnakeActor->LevelName = TEXT("Two");
		break;
	case ESnakeLevel::THREE:
		SnakeActor->LevelName = TEXT("Three");
		break;
	case ESnakeLevel::FOUR:
		SnakeActor->LevelName = TEXT("Four");
		break;
	case ESnakeLevel::FIVE:
		SnakeActor->LevelName = TEXT("Five");
		break;
	default:
		SnakeActor->LevelName = TEXT("One");
		break;
	}
	int32 FoodCount_ = FoodCount;
	SnakeActor->MapSize = X_Map * Y_Map;
	SnakeActor->FoodCount = FoodCount_;
	SnakeActor->FoodForWin = FoodForWin;
	SnakeActor->MaxFoodWin(MaxFood);
	SnakeActor->ImmortalSnake = ImmortalSnake;
	SnakeActor->PlayerPawn = this;
	SnakeActor->MapTeleport_X = FloorSize * X_Map;
	SnakeActor->MapTeleport_Y = FloorSize * Y_Map;
	SnakeActor->MovementSpeed = SnakeMovementSpeed;
	while (FoodCount_)
	{
		SnakeActor->CreateFood();
		FoodCount_--;
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if (!SnakeActor->SnakeDead)
		{
		if (value > 0 && SnakeActor->CheckMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			SnakeActor->SnakeElements[0]->MeshComponent->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));
		}
		else if (value < 0 && SnakeActor->CheckMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->SnakeElements[0]->MeshComponent->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
		}

		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if (!SnakeActor->SnakeDead)
		{
			if (value > 0 && SnakeActor->CheckMoveDirection != EMovementDirection::LEFT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
				SnakeActor->SnakeElements[0]->MeshComponent->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
			}
			else if (value < 0 && SnakeActor->CheckMoveDirection != EMovementDirection::RIGHT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
				SnakeActor->SnakeElements[0]->MeshComponent->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerExitInput()
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->Destroy();
	}
	for (int i = 0; i < Floors.Num(); i++)
	{
		if (IsValid(Floors[i]))
		{
			Floors[i]->Destroy();
		}
	}
	Floors.Empty();
	for (int i = 0; i < Walls.Num(); i++)
	{
		if (IsValid(Walls[i]))
		{
			Walls[i]->Destroy();
		}
	}
	Walls.Empty();
	X_Map = 11;
	Y_Map = 11;
	ChoiceLevel = ESnakeLevel::ONE;
	FoodCount = 1;
	FoodForWin = 10;
	WallGame = true;
	MapSize = X_Map * Y_Map;
	MaxFood = false;
	MovementSpeed = 0.5f;
	SnakeMovementSpeed = MovementSpeed;
	ImmortalSnake = false;
	StartSnake();
}

void APlayerPawnBase::NextLevel()
{
	SnakeActor->Destroy();
		for (int32 i = 0; i < Floors.Num(); i++)
		{
			Floors[i]->Destroy();
		}
		Floors.Empty();
		for (int32 i = 0; i < Walls.Num(); i++)
		{
			Walls[i]->Destroy();
		}
		Walls.Empty();
		int32 EnumIndex;
		EnumIndex = static_cast<int32>(ChoiceLevel);
		EnumIndex++;
		ChoiceLevel = static_cast<ESnakeLevel>(EnumIndex);
		DrowLevel();
		CreateSnakeActor();
}

void APlayerPawnBase::SnakeAllWin_Implementation()
{

}

void APlayerPawnBase::StartSnake_Implementation()
{

}

void APlayerPawnBase::ChooseLevel(int32& level)
{
	level--;
	if (level >= 0 && level < 5)
	{
		ChoiceLevel = static_cast<ESnakeLevel>(level);
	}
}



void APlayerPawnBase::ChangeSpeed(float& value)
{
	SnakeMovementSpeed = MovementSpeed / value;
}


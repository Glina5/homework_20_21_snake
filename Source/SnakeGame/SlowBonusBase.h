// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "SlowBonusBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASlowBonusBase : public ABonusBase
{
	GENERATED_BODY()

public:
	ASlowBonusBase();
};
